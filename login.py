# coding:utf-8
import time
import re
import pickle
import logging
import os
from pprint import pprint
from os import path

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
import selenium.common.exceptions as CEC

from driver import create_driver

class SinaDriver(object):
    def __init__(self, account, password, proxy=None, headless=False):
        self.account = account
        self.password = password
        self.cookie_filename = 'cookie/'+re.sub('[^0-9a-zA-Z_]', '', account)+'.pkl'
        self.retry_login = 3
        logging.info('初始化Chrome')
        self.driver = create_driver(proxy, headless)
        # 打开登录页面
        self.driver.delete_all_cookies()
        self.driver.get("http://www.weibo.com")
        self.login()
    
    def login(self):
        # 如果cookie可用，直接使用cookie登录
        if path.exists(self.cookie_filename):
            with open(self.cookie_filename, 'rb') as cookie_file:
                cookies = pickle.load(cookie_file)
                for c in cookies:
                    self.driver.add_cookie(c)
            self.driver.refresh()
            try:
                WebDriverWait(self.driver, 30).until(lambda driver: "CONFIG['islogin']='1'" in self.driver.page_source)
                logging.info('账号: '+self.account+' cookie可用')
                return
            except CEC.TimeoutException:
                logging.info('账号: '+self.account+' cookie不可用，重新登录中...')
                # 移除过期cookie
                os.remove(self.cookie_filename)
        # 输入账号密码
        try:
            name_field = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'loginname')))
            password_field = WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.XPATH, '//*[@node-type="password"]')))
            name_field.clear()
            name_field.send_keys(self.account)
            time.sleep(1)
            password_field.clear()
            password_field.send_keys(self.password)
            time.sleep(1)
        except CEC.TimeoutException:
            logging.info("登录界面打开超时")
        # 点击登录
        try:
            submit_btn = WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.CLASS_NAME, "W_btn_a")))
            submit_btn.click()
        except CEC.TimeoutException:
            logging.info("无法点击登录按钮")
        time.sleep(2)
        while self.retry_login > 0:
            # 检查是否需要验证码
            if len(self.driver.find_elements_by_name('verifycode')) > 0:
                self.input_verifycode()
                submit_btn.click()
                time.sleep(2)
            # 判断是否密码错误
            if self.password_err():
                logging.info("账号："+self.account+" 账号密码错误")
                self.driver.quit()
                return
            # 检查网络是否超时
            if self.timeout_err():
                self.retry_login -= 1
                continue
            # 检查是否登录成功
            if self.login_successfully():
                weibo_cookies = self.driver.get_cookies()
                cookie_filename = re.sub('[^0-9a-zA-Z_]', '', self.account)
                pickle.dump(weibo_cookies, open(self.cookie_filename, "wb"))
                logging.info("账号："+self.account+" 登录成功，cookie已保存")
                break
            self.retry_login -= 1
        if self.retry_login < 0:
            logging.info("账号："+self.account+" 验证码错误，登录失败")
            self.driver.quit()
            return
        return self.driver

    def input_verifycode(self):
        try:
            # TODO: 尝试使用百度服务直接识别验证码
            WebDriverWait(self.driver, 30).until(
                EC.visibility_of_element_located((By.NAME, "verifycode")))
            self.driver.find_element_by_name("verifycode").send_keys(input('输入验证码:'))
        except CEC.TimeoutException:
            return
    
    def password_err(self):
        try:
            return WebDriverWait(self.driver, 5).until(
                EC.text_to_be_present_in_element((By.CLASS_NAME, 'W_layer_pop'), '用户名或密码错误'))
        except CEC.TimeoutException:
            return False
    
    def timeout_err(self):
        try:
            return WebDriverWait(self.driver, 5).until(
                lambda x: '超时' in x.page_source)
        except CEC.TimeoutException:
            return False

    def login_successfully(self):
        try:
            return WebDriverWait(self.driver, 30).until(
                    lambda x: "CONFIG['islogin']='1'" in x.page_source)
        except CEC.TimeoutException:
            return False

if __name__ == '__main__':
    SinaDriver(account="18476089039", password="8qsdf9xmyk")
