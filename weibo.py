# coding:utf-8
import time
import re
import pickle
import os
import random
import string
import logging
from pprint import pprint
from os import path
from datetime import datetime
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
import selenium.common.exceptions as CEC

from login import SinaDriver

with open('comment.txt', 'r') as f:
    comment_texts = f.readlines()

class Weibo(object):
    def __init__(self, driver):
        self.driver = driver.driver
        self.cookie_filename = driver.cookie_filename
        with open(driver.cookie_filename, 'rb') as f:
            self.cookie = pickle.load(f)
        if 'status' in self.cookie[-1].keys():
            self.status = self.cookie[-1]['status']
            self.cookie.pop()
        else:
            self.status = 0

    def weibo_post(self, link, user_name, 
                   comment_texts=["评论微博"], forward_texts=["转发微博"]):
        '''
        :param link: 单条微博直链
        :param user_name: 与指定用户互动
        '''
        # 打开微博
        self.driver.get(link)
        try:
            WebDriverWait(self.driver, 30).until(lambda driver: '<div class="WB_feed_handle"' in driver.page_source)
        except CEC.TimeoutException:
            logging.info("微博："+link+"打开失败")
        # 微博po主名字
        poster_name = self.driver.execute_script("return $CONFIG.onick")
        # 如果微博po主是互动对象，则点赞评
        if user_name == poster_name:
            logging.info("微博po主是互动对象")
            # 检查是否点过赞
            liked_before = False
            try:
                like_btn = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.XPATH, '//*[@class="WB_feed_handle"]//*[@action-type="fl_like"]')))
                if like_btn.get_attribute('title') == '赞':
                    self.driver.execute_script("return arguments[0].scrollIntoView();", like_btn)
                    self.driver.execute_script("window.scrollBy(0, -150);")
                    ActionChains(self.driver).move_to_element(like_btn).click().perform()
                    time.sleep(3)
                else:
                    liked_before = True
            except CEC.TimeoutException:
                logging.info("微博："+link+"点赞失败")
            # 如果没点过赞则假设未转评过，如果点过赞则假设已经转评过
            if not liked_before:
                cmt_btn = self.driver.find_element_by_xpath('//*[@class="WB_feed_handle"]//*[@action-type="fl_comment"]')

                ActionChains(self.driver).move_to_element(cmt_btn).click().perform()
                time.sleep(random.randint(3,6))
                try:
                    # 评论
                    comment_textarea = WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located((By.XPATH, '//*[@class="WB_frame_c"]//*[@node-type="comment_detail"]//*[@class="W_input"]')))
                    comment_textarea.send_keys(random.choice(comment_texts))
                    time.sleep(random.randint(3,6))
                    comment_btn = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.XPATH, '//*[@class="WB_frame_c"]//*[@node-type="comment_detail"]//*[@class="W_btn_a"]')))
                    self.driver.execute_script("return arguments[0].scrollIntoView();", comment_btn)
                    self.driver.execute_script("window.scrollBy(0, -150);")
                    ActionChains(self.driver).move_to_element(comment_btn).click().perform()
                    time.sleep(random.randint(8,12))
                except CEC.TimeoutException or CEC.StaleElementReferenceException:
                    logging.info('评论失败')
                try: 
                    # 转发
                    fwd_btn = self.driver.find_element_by_xpath('//*[@class="WB_feed_handle"]//*[@action-type="fl_forward"]')
                    self.driver.execute_script("return arguments[0].scrollIntoView();", fwd_btn)
                    self.driver.execute_script("window.scrollBy(0, -150);")
                    ActionChains(self.driver).move_to_element(fwd_btn).click().perform()
                    forward_textarea = WebDriverWait(self.driver, 30).until(EC.visibility_of_element_located((By.XPATH, '//*[@class="WB_frame_c"]//*[@node-type="forward_detail"]//*[@class="W_input"]')))
                    forward_textarea.send_keys(random.choice(forward_texts))
                    time.sleep(random.randint(3,6))
                    forward_btn = self.driver.find_element_by_xpath('//*[@class="WB_frame_c"]//*[@node-type="forward_detail"]//*[@class="W_btn_a"]')
                    ActionChains(self.driver).move_to_element(forward_btn).click().perform()
                    time.sleep(random.randint(8,12))
                except CEC.TimeoutException:
                    logging.info('转发失败')
        # 与一二级评论互动
        # FIXME：目前只能寻找第一页的一级评论与最外部的二级评论
        cmt_btn = self.driver.find_element_by_xpath('//*[@class="WB_feed_handle"]//*[@action-type="fl_comment"]')
        self.driver.execute_script("return arguments[0].scrollIntoView();", cmt_btn)
        self.driver.execute_script("window.scrollBy(0, -150);")
        ActionChains(self.driver).move_to_element(cmt_btn).click().perform()
        time.sleep(random.randint(3,6))
        try:
            WebDriverWait(self.driver, 15).until(EC.visibility_of_element_located((By.XPATH, '//*[@class="WB_frame_c"]//*[@node-type="comment_detail"]//*[@node-type="root_comment"]')))
            root_lists = self.driver.find_elements_by_xpath('//*[@class="WB_frame_c"]//*[@node-type="comment_detail"]//*[@node-type="root_comment"]')
        except CEC.TimeoutException:
            logging.info("无法显示一级评论")
            root_lists = []
        try:
            WebDriverWait(self.driver, 15).until(EC.presence_of_element_located((By.XPATH, '//*[@class="WB_frame_c"]//*[@node-type="comment_detail"]//*[@node-type="child_comment"]')))
            child_lists = self.driver.find_elements_by_xpath('//*[@class="WB_frame_c"]//*[@node-type="comment_detail"]//*[@node-type="child_comment"]/*[@comment_id]')
        except CEC.TimeoutException:
            logging.info("无法显示二级评论")
            child_lists = []
        comment_lists = root_lists + child_lists
        for comment in comment_lists:
            try:
                comment_user = comment.find_element_by_class_name("WB_text").find_element_by_tag_name('a').get_attribute('innerHTML')
            except CEC.NoSuchElementException:
                continue
            if comment_user == user_name:
                like_btn = comment.find_element_by_xpath('.//a[@action-type="fl_like"]')
                if like_btn.get_attribute('title') == '赞':
                    # 点赞
                    self.driver.execute_script("return arguments[0].scrollIntoView();", like_btn)
                    self.driver.execute_script("window.scrollBy(0, -150);")
                    ActionChains(self.driver).move_to_element(like_btn).click().perform()
                    time.sleep(random.randint(3,6))
                    # 回复
                    reply_btn = comment.find_element_by_xpath('.//a[@action-type="reply"]')
                    ActionChains(self.driver).move_to_element(reply_btn).click().perform()
                    try: 
                        WebDriverWait(self.driver, 30).until(lambda x: 'doReply' in comment.get_attribute('innerHTML'))
                        time.sleep(1)
                        cmt_area = comment.find_element_by_xpath('.//textarea')
                        cmt_area.send_keys(random.choice(comment_texts))
                        time.sleep(1)
                        btn = WebDriverWait(self.driver, 30).until(EC.visibility_of(comment.find_element_by_xpath('.//*[@action-type="doReply"]')))
                        ActionChains(self.driver).move_to_element(btn).click().perform()
                        time.sleep(random.randint(8,12))
                    except CEC.TimeoutException:
                        logging.info("内部评论失败")
        logging.info("微博互动任务完成")

    def weibo_gushi(self):
        '''
        TODO: 什么是微博故事？
        '''
        pass
    
    def weibo_qun(self):
        '''
        TODO: 
        '''
        pass

    def follow_up(self, link):
        '''
        关注微博
        :param link: 微博主页链接
        '''
        self.driver.get(link)
        retry = 3
        while retry > 0:
            try:
                follow_btn = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.XPATH,'//*[@class="pf_opt"]/div/div/a')))
                if '+关注' in follow_btn.get_attribute('text'):
                    follow_btn.click()
                logging.info("关注"+link+"成功")
                break
            except CEC.TimeoutException:
                retry-=1
                time.sleep(random.randint(3,6))
        if retry < 0:
            logging.info("关注"+link+"失败")

    def post_original(self, original_text):
        '''
        发布原创微博
        :param original_text: 原创微博内容
        '''
        self.driver.get("http://www.weibo.com")
        try:
            input_area = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.XPATH, '//*[@class="WB_main_c"]//*[@class="W_input"]')))
            input_area.send_keys(original_text)
            time.sleep(random.randint(3,6))
            submit_btn = WebDriverWait(self.driver, 30).until(EC.element_to_be_clickable((By.XPATH, '//*[@title="发布微博按钮"]')))
            self.driver.find_element_by_xpath('//*[@title="发布微博按钮"]').send_keys(Keys.ENTER)
            logging.info("发送原创微博成功")
        except CEC.TimeoutException:
            logging.info("发送原创微博失败")

    # def checkin_supertopic(self, link):
    #     '''
    #     超话签到
    #     :param link: 超话首页链接
    #     '''
    #     self.driver.get(link)
    #     try:
    #         follow_btn = WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.XPATH, '//*[@node-type="followBtnBox"]/a')))
    #         if follow_btn.get_attribute('action-type') == 'follow':
    #             WebDriverWait(self.driver, 30).until(EC.element_to_be_clickable((By.XPATH, '//*[@node-type="followBtnBox"]/a')))
    #             follow_btn.click()
    #             time.sleep(random.randint(3,6))
    #         WebDriverWait(self.driver, 30).until(EC.presence_of_element_located((By.XPATH,'//*[@action-type="widget_take"]')))
    #         WebDriverWait(self.driver, 30).until(EC.element_to_be_clickable((By.XPATH,'//*[@action-type="widget_take"]')))
    #         checkin_btn = self.driver.find_element_by_xpath('//*[@action-type="widget_take"]')
    #         ActionChains(self.driver).move_to_element(checkin_btn).click().perform()
    #         time.sleep(3)
    #         logging.info("超话签到成功")
    #     except CEC.TimeoutException:
    #         logging.info("超话签到失败")

if __name__ == '__main__':
    driver = SinaDriver(account="13620241821", password="4nzp8xokkp")
    weibo = Weibo(driver)
    print("原创")
    weibo.post_original(random.choice(comment_texts))
    time.sleep(random.randint(3,6))
    print("关注")
    weibo.follow_up("https://www.weibo.com/breakingnews?profile_ftype=1&is_all=1#_0")
    time.sleep(random.randint(3,6))
    print("互动")
    weibo.weibo_post("https://weibo.com/1649818571/JaibaypVT?from=page_1005051649818571_profile&wvr=6&mod=weibotime&type=comment#_rnd1594245117270", 
                     "曾可妮Jenny-Z", comment_texts, comment_texts)
    time.sleep(random.randint(3,6))
    # print("超话")
    # weibo.checkin_supertopic("https://www.weibo.com/p/10080893d1743c52fe7bcf7e3aff72dc58f770/super_index")
    time.sleep(3)
    weibo.driver.quit()
