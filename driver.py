from random import choice
from selenium import webdriver

def generate_ua():
    '''
    生成随机userAgent
    '''
    user_agent_system = [
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Windows NT 10.0) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_5) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_5) AppleWebKit/537.36 (KHTML, like Gecko)',
        'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko)'
    ]
    user_agent_browser = [
        'Chrome/83.0.4103.116 Safari/537.36',
        'Chrome/79.0.3945.117 Safari/537.36',
        'Chrome/74.0.3729.169 Safari/537.36',
        'Chrome/73.0.3729.157 Safari/537.36',
        'Chrome/72.0.3626.121 Safari/537.36',
        'Chrome/70.0.3538.102 Safari/537.36',
        'Chrome/69.0.3497.100 Safari/537.36'
        'Chrome/67.0.3396.87 Safari/537.36',
        'Chrome/60.0.3112.90 Safari/537.36',
        'Chrome/57.0.2987.133 Safari/537.36',
        'Chrome/55.0.2883.87 Safari/537.36',
        'Chrome/54.0.2840.99 Safari/537.36'
    ]
    return choice(user_agent_system) + " " + choice(user_agent_browser)

def create_driver(proxy_ip=None, headless=False):
    '''
    打开浏览器
    :param proxy_ip: IP地址
    '''
    options = webdriver.ChromeOptions() 
    options.add_argument("window-size=1200,900")
    if headless:
        options.add_argument("--headless")
    options.add_argument('--ignore-certificate-errors')
    options.add_argument('--ignore-ssl-errors')
    options.add_argument('--log-level=3')
    options.add_argument('disable-infobars')
    options.add_experimental_option("excludeSwitches", ["enable-automation"])
    options.add_experimental_option('useAutomationExtension', False)
    if proxy_ip is not None:
        options.add_argument('--proxy-server=%s' % proxy_ip)
    driver = webdriver.Chrome(executable_path='./chromedriver', 
                              options=options)
    driver.execute_cdp_cmd("Page.addScriptToEvaluateOnNewDocument", {
      "source": """
        Object.defineProperty(navigator, 'webdriver', {
          get: () => undefined
        })
      """
    })
    ua = generate_ua()
    driver.execute_cdp_cmd('Network.setUserAgentOverride', {"userAgent": ua})
    return driver
